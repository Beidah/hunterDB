#include "InputBuffer.hpp"

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <iterator>

void InputBuffer::ReadInput()
{
	std::getline(std::cin, m_Buffer);
}

MetaCommandResult InputBuffer::MetaCommand(Table *table)
{
	if (m_Buffer.compare(".exit") == 0)
	{
		table->CloseDatabase();
		exit(EXIT_SUCCESS);
	}
	else
	{
		return MetaCommandResult::Unreconized;
	}
}

PrepareResult InputBuffer::PrepareStatement(Statement &statement)
{
	if (m_Buffer.substr(0, 6) == "insert")
	{
		return prepareInsert(statement);
	}

	if (m_Buffer.substr(0, 6) == "select")
	{
		statement = Statement(StatementType::Select);
		return PrepareResult::Success;
	}
	statement = Statement();

	return PrepareResult::Unreconized;
}

PrepareResult InputBuffer::prepareInsert(Statement &statement)
{
	Row row;

	std::istringstream iss(m_Buffer);
	std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
									std::istream_iterator<std::string>{}};

	if (tokens.size() > 4)
	{
		return PrepareResult::SyntaxError;
	}

	const char *id_string = tokens.at(1).c_str();
	const char *username = tokens.at(2).c_str();
	const char *email = tokens.at(3).c_str();

	if (id_string == nullptr || username == nullptr || email == nullptr)
	{
		printf("Error: (%s, %s, %s)\n", id_string, username, email);
		return PrepareResult::SyntaxError;
	}

	int id = atoi(id_string);
	if (id < 0)
		return PrepareResult::NegativeId;

	if (strlen(username) > COLUMN_USERNAME_SIZE || strlen(email) > COLUMN_EMAIL_SIZE)
	{
		return PrepareResult::StringTooLong;
	}

	row.id = id;
	strcpy_s(row.username, username);
	strcpy_s(row.email, email);

	statement = Statement(StatementType::Insert, row);

	return PrepareResult::Success;
}
