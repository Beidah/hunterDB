#include "Table.hpp"

#include <cstdio>

void Table::OpenDatabase(const char *filePath)
{
    m_Pager = new Pager(filePath);
    m_NumberOfRows = m_Pager->GetFileLength() / ROW_SIZE;
}

void Table::CloseDatabase()
{
    u_size fullPages = m_NumberOfRows / ROWS_PER_PAGE;

    for (u_size i = 0; i < fullPages; i++)
    {
        Page *page = m_Pager->GetPage(i);
        if (page == nullptr)
            continue;
        m_Pager->Flush(i, PAGE_SIZE);
    }

    // There may be a partial page to write to the end of the file
    // This should not be needed after we switch to a B-tree
    u_size additionalRows = m_NumberOfRows % ROWS_PER_PAGE;
    if (additionalRows > 0)
    {
        m_Pager->Flush(fullPages, additionalRows * ROW_SIZE);
    }

    delete m_Pager;
}

void Table::AddRow(Row row)
{
    row.Serialize(End()->Value());
    m_NumberOfRows++;
}

Row *Table::retrieveRowPtr(u_size rowNumber)
{
    u_size pageNumber = rowNumber / ROWS_PER_PAGE;
    Page *page = m_Pager->GetPage(pageNumber);

    Row *row = &page->rows[rowNumber % ROWS_PER_PAGE];

    if (row == nullptr) {
        row = new Row();
    }
    
    return row;
}

Row Table::retrieveRow(u_size rowNumber) {
    u_size pageNumber = rowNumber / ROWS_PER_PAGE;
    Page *page = m_Pager->GetPage(pageNumber);

    Row row = page->rows[rowNumber % ROWS_PER_PAGE];
    return row;
}

Cursor::Cursor(Table* table, u_size rowNumber)  :
 m_Table(table), m_RowNumber(rowNumber) 
{ 
    m_EndOfTable = rowNumber >= m_Table->m_NumberOfRows; 
}

void Cursor::Advance() {
    m_RowNumber++;
    if (m_RowNumber >= m_Table->m_NumberOfRows) {
        m_EndOfTable = true;
    }
}

Row* Cursor::Value() {
    Page* page = m_Table->m_Pager->GetPage(m_RowNumber / ROWS_PER_PAGE);
    Row* row = &page->rows[m_RowNumber % ROWS_PER_PAGE];

    if (row == nullptr) {
        row = new Row();
        page->rows[m_RowNumber % ROWS_PER_PAGE] = *row;
    }

    return row;
}