#include "Page.hpp"

#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <stdio.h>
#include <stdlib.h>

//#define SH_DENYRW 0x10

void Row::Serialize(Row *destination)
{
    *destination = *this;
}

void Row::Print()
{
    printf("(%d, %s, %s)\n", id, username, email);
}

Pager::Pager(const char *filepath)
{
    _sopen_s(&m_FileDescriptor, filepath, O_RDWR | O_CREAT, SH_DENYRW, S_IREAD | S_IWRITE);

    if (m_FileDescriptor == -1)
    {
        printf("Unable to open file: %s\n", filepath);
        exit(EXIT_FAILURE);
    }
    m_FileLength = _lseek(m_FileDescriptor, 0, SEEK_END);

    for (u_size i = 0; i < TABLE_MAX_PAGES; i++) {
        m_Pages[i] = nullptr;
    }
}

Pager::~Pager()
{
    int result = _close(m_FileDescriptor);
    if (result == -1)
    {
        printf("Error closing db file.\n");
        exit(EXIT_FAILURE);
    }
}

void Pager::Flush(u_size pageNumber, size_t size)
{
    if (m_Pages[pageNumber] == nullptr)
    {
        printf("Tried to flush null page.\n");
        exit(EXIT_FAILURE);
    }

    off_t offset = _lseek(m_FileDescriptor, pageNumber * PAGE_SIZE, SEEK_SET);
    if (offset == -1)
    {
        printf("Error seeking: %d\n", errno);
        exit(EXIT_FAILURE);
    }

    int bytesWritten = _write(m_FileDescriptor, m_Pages[pageNumber], size);
    if (bytesWritten == -1)
    {
        printf("Error writing: %d\n", errno);
        exit(EXIT_FAILURE);
    }
}

Page *Pager::GetPage(u_size pageNumber)
{
    if (pageNumber > TABLE_MAX_PAGES)
    {
        printf("Tried to fetch page number out of bounds. %d > %d\n", pageNumber, TABLE_MAX_PAGES);
        exit(EXIT_FAILURE);
    }

    if (m_Pages[pageNumber] == nullptr)
    {
        Page *page = new Page();

        u_size numberOfPages = m_FileLength / PAGE_SIZE;

        if (m_FileLength % PAGE_SIZE)
            numberOfPages++;

        if (pageNumber <= numberOfPages)
        {
            _lseek(m_FileDescriptor, pageNumber * PAGE_SIZE, SEEK_SET);
            long bytesRead = _read(m_FileDescriptor, page, PAGE_SIZE);
            if (bytesRead == -1)
            {
                printf("Error reading file: %d\n", errno);
                exit(EXIT_FAILURE);
            }
        }

        m_Pages[pageNumber] = page;
    }

    return m_Pages[pageNumber];
}
